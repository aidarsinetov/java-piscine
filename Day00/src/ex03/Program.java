package ex03;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int i = 0;
        long mins = 0;
        for (; i < 18 && flag; i++) {
            String in = scanner.nextLine();
            if (in.equals("42"))
                break;
            if (!in.equals("Week " + (i + 1)))
                exit();

            int min = 9;
            for (int j = 0; j < 5; j++) {
                int n = scanner.nextInt();
                if (n == 42) {
                    flag = false;
                    break;
                }
                if (n < 1 || n > 9)
                    exit();
                min = n < min ? n : min;
            }
            scanner.nextLine();
            mins *= 10;
            mins += min;
        }

        long revMins  = reverseNbr(mins);
        for (int j = 0; j < i; j++) {
            System.out.print("Week " + (j + 1) + " ");
            int nbr = (int) (revMins % 10);
            revMins /= 10;
            for (int k = 0; k < nbr; k++)
                System.out.print("=");
            System.out.println(">");
        }
    }

    private static void exit() {
        System.err.println("IllegalArgument");
        System.exit(-1);
    }

    private static long reverseNbr(long number) {
        long n = 0;
        if (number < 10)
            return number;
        do {
            n += number % 10;
            n *= 10;
            number /= 10;
        } while (number / 10 >= 1);
        n += number;
        return n;
    }
}
