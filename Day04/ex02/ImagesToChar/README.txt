#!/bin/bash

# Deleting generated files
if [ -d target ] ;
then
    rm -rf target/;
fi
if [ -d lib ] ; then
    rm -rf lib/;
fi

# Creating necessary directories: 'target' for output files and 'lib' to libraries
mkdir -p target lib

# Download necessary libraries: JCommander and JColor
$(cd lib && curl -s -O https://repo1.maven.org/maven2/com/beust/jcommander/1.82/jcommander-1.82.jar)
$(cd lib && curl -s -O https://repo1.maven.org/maven2/com/diogonunes/JColor/5.3.0/JColor-5.3.0.jar)

# Unzip downloaded libraries to target/
cp lib/* target/
jars=$(ls target/*.jar)
for arch in $jars
do
  $(cd target/ && jar xf $(basename $arch));
  rm -rf $arch;
done
rm -rf target/META-INF

# Compiling sources
javac -d target $(find src -name "*.java") -cp target/

# Coping resources
cp -R src/resources target/

# Making jar archive with project in folder target/
jar -cfm ./target/images-to-chars-printer.jar ./src/manifest.txt -C target .

# Run built archive
java -jar ./target/images-to-chars-printer.jar
