package edu.school21.sockets.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import edu.school21.sockets.models.User;
import edu.school21.sockets.repositories.UsersRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Random;

@Service
public class UsersServiceImpl implements UsersService{
    @Autowired
    @Qualifier("usersRepositoryImpl")
    private UsersRepository usersRepository;
    private final static String availableSymbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-#*@abcdefghijklmnopqrstuvwxyz";
    private final PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public String signUp(User user) {
        Objects.requireNonNull(user);
        if (user.getId() == null) {
            if (user.getPassword() == null || user.getPassword().isEmpty()) {
                String password = generatePassword();
                user.setPassword(encoder.encode(password));
            }
            usersRepository.save(user);
        }
        return user.getPassword();
    }

    private String generatePassword() {
        Random rnd = new Random();

        StringBuilder sb = new StringBuilder(9 + rnd.nextInt(11));
        for (int i = 0; i < sb.capacity(); i++) {
            sb.append(availableSymbols.charAt(rnd.nextInt(availableSymbols.length())));
        }
        return sb.toString();
    }

    public String getAvailableSymbols() {
        return availableSymbols;
    }

    public static boolean checkPassword(String password){
        char[] passwordChars = password.toCharArray();
        char[] availableSymbols = UsersServiceImpl.availableSymbols.toCharArray();
        for (char ch : passwordChars) {
            boolean success = false;
            for (char ch1 : availableSymbols) {
                success |= ch1 == ch;
            }
            if (success == false)
                return false;
        }
        return true;
    }
}
