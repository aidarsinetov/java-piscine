package edu.school21.chat.models;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class Message {
    private long id;
    private User author;
    private Chatroom chatroom;
    private String text;
    private Date date;
    private Time time;

    public Message() {
    }

    public Message(long id, User author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public Message setId(long id) {
        this.id = id;
        return this;
    }

    public User getAuthor() {
        return author;
    }

    public Message setAuthor(User author) {
        this.author = author;
        return this;
    }

    public Chatroom getChatroom() {
        return chatroom;
    }

    public Message setChatroom(Chatroom chatroom) {
        this.chatroom = chatroom;
        return this;
    }

    public String getText() {
        return text;
    }

    public Message setText(String text) {
        this.text = text;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public Message setDate(Date date) {
        this.date = date;
        return this;
    }

    public Time getTime() {
        return time;
    }

    public Message setTime(Time time) {
        this.time = time;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return id == message.id
                && Objects.equals(author, message.author)
                && Objects.equals(text, message.text)
                && Objects.equals(date, message.date)
                && Objects.equals(time, message.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, text, date, time);
    }

    @Override
    public String toString() {
        return "Message : { \n" +
                "id=" + id + ", \n" +
                "author=" + author + ", \n" +
                "chatroom=" + chatroom + ", \n" +
                "text='" + text + '\'' + ", \n" +
                "date=" + date + ", \n" +
                "time=" + time + ", \n" +
                '}';
    }
}
