package edu.school21.spring;

import sun.util.resources.LocaleData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class PrinterWithDateTimeImpl implements Printer{
    private LocalDateTime date;
    private Renderer renderer;


    public PrinterWithDateTimeImpl(Renderer renderer) {
        date = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        this.renderer = renderer;
    }

    public PrinterWithDateTimeImpl(LocalDateTime date, Renderer renderer) {
        this.date = date;
        this.renderer = renderer;
    }

    public PrinterWithDateTimeImpl(String dateTime, Renderer renderer) {
        this.date = LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
        this.renderer = renderer;
    }

    @Override
    public void print(String str) {
        renderer.render(date.toString() + " " + str);
    }
}
