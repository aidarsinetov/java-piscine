package edu.school21.chat.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;
import edu.school21.chat.repositories.ChatroomRepositoryJdbcImpl;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;
import edu.school21.chat.repositories.UserRepositoryJdbcImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Scanner;

public class Program {
	public static final String DB_URL = "jdbc:postgresql://localhost/postgres";
	public static final String DB_USER = "postgres";
	public static final String DB_PWD = "postgres";
	private static final String DB_SCHEMA = "schema.sql";
	private static final String DB_DATA = "data.sql";

	public static void main(String[] args) throws SQLException{
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(DB_URL);
		config.setUsername(DB_USER);
		config.setPassword(DB_PWD);
		DataSource dataSource = new HikariDataSource(config);

		Connection connection = dataSource.getConnection();
		execSQLFile(connection, ClassLoader.getSystemResourceAsStream(DB_SCHEMA));
		execSQLFile(connection, ClassLoader.getSystemResourceAsStream(DB_DATA));
		connection.close();

		UserRepositoryJdbcImpl userRepos = new UserRepositoryJdbcImpl(dataSource);
		User usr = new User().setLogin("Pushkin").setPassword("dddd");
		userRepos.save(usr);

		ChatroomRepositoryJdbcImpl chroomRepo = new ChatroomRepositoryJdbcImpl(dataSource);
		Chatroom room1 = new Chatroom().setName("21 Hostel").setOwner(usr);
		chroomRepo.save(room1);

		long time1 = new java.util.Date().getTime();
		MessagesRepositoryJdbcImpl msgRepo = new MessagesRepositoryJdbcImpl(dataSource);
		Message msg = new Message().setAuthor(usr)
									.setChatroom(room1).setText("I'm a Pushkin in hostel 21")
									.setDate(new Date(time1)).setTime(new Time(time1));
		msgRepo.save(msg);

		Chatroom room2 = new Chatroom().setName("progers").setOwner(usr);
		chroomRepo.save(room2);
		msg.setChatroom(room2).setAuthor(null);
		msgRepo.update(msg);
	}

	private static void execSQLFile(Connection con, InputStream in) throws SQLException {
		Scanner sc = new Scanner(in).useDelimiter(";");
		Statement statement = con.createStatement();
		while (sc.hasNext()) {
			String st = sc.next().replaceAll("\n", "");
			statement.execute(st + ";");
		}

	}
}
