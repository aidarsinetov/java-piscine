package edu.school21.chat.repositories;

import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
	List<User> findAll(int page, int size);
	Optional<User> findById(Long id);
	boolean save(User user);
	boolean update(User user);
}
