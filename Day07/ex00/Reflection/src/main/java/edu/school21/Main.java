package edu.school21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Class> classes = new ArrayList<>(Arrays.asList(Car.class,  User.class));
        Scanner scanner = new Scanner(System.in);
        System.out.println("Classes:");
        classes.forEach(x -> System.out.println(x.getSimpleName()));
        System.out.println("---------------------");
        System.out.println("Enter class name:");
        String string = scanner.nextLine();
    }
}
