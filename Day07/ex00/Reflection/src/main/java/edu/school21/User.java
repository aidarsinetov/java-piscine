package edu.school21;

public class User {
    private String name;
    private int age;
    private int height;
    private int weight;

    public User() {
    }

    public User(String name, int age, int height, int weight) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
    }

    public int grow(){
        age++;
        height++;
        return age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }
}
