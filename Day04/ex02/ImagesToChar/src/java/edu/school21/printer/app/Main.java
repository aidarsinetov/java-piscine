package edu.school21.printer.app;

import com.beust.jcommander.JCommander;
import com.diogonunes.jcolor.Ansi;
import com.diogonunes.jcolor.Attribute;
import edu.school21.printer.logic.ImageConverter;
import java.awt.*;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        JCommander.newBuilder().addObject(new Args()).build().parse(args);
        try {
            int[][] img = new ImageConverter(Args.getWhiteReplaced(), Args.getBlackReplaced(), ClassLoader.getSystemResourceAsStream("resources/it.bmp")).convert();
            Color curColor;
            for (int i = 0; i < img.length; i++) {
                for (int j = 0; j < img[0].length; j++)
                {
                    curColor = new Color(img[j][i]);
                    System.out.print(Ansi.colorize(" ", Attribute.BACK_COLOR(curColor.getRed(), curColor.getGreen(), curColor.getBlue())));
                }
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
