package edu.school21.repositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import java.sql.Connection;
import java.sql.SQLException;

public class EmbeddedDataSourceTest {

    EmbeddedDatabase db;

    @BeforeEach
    void init(){
       db =  new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.HSQL)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScript("/schema.sql")
                .addScript("/data.sql")
                .build();
    };


    @Test
    void getConnection(){
        try {
            Connection connection = db.getConnection();
            Assertions.assertNotNull(connection);
            db.shutdown();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
