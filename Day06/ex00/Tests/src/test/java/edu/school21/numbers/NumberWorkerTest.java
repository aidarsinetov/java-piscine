package edu.school21.numbers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

public class NumberWorkerTest {
    NumberWorker numberWorker;

    @BeforeEach
    public void createNumberWorker() {
        numberWorker = new NumberWorker();
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 5, 7, 11, 13, 17})
    public void isPrimeForPrimes(int param) {
        Assertions.assertTrue(numberWorker.isPrime(param));
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 6, 8, 9, 10, 155})
    public void  isPrimeForNotPrimes(int param) {
        Assertions.assertFalse(numberWorker.isPrime(param));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, -2, 0, 1, -10})
    public void  isPrimeForIncorrectNumbers(int param) {
        Assertions.assertThrows(IllegalNumberException.class , ()-> numberWorker.isPrime(param));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv", delimiter = ',')
    public void testDigitsSum(int num, int sum) {
        Assertions.assertEquals(sum, numberWorker.digitsSum(num));
    }
}
