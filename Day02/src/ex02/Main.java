import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class Main {
    private static Path currentPath;

    public static void main(String[] args) {
        if (args.length != 1 || !args[0].startsWith("--current-folder=")) {
            System.err.println("usage: Program --current-folder=FOLDERNAME");
            return;
        }
        currentPath = Paths.get(args[0].substring(args[0].indexOf('=') + 1));
        checkPath(currentPath, "", true);
        System.out.println(currentPath);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String[] cmd = scanner.nextLine().trim().split(" ");
            if (cmd[0].trim().equals("exit"))
                System.exit(0);
            else if (cmd[0].trim().equals("ls"))
                ls(currentPath);
            else if (cmd[0].trim().equals("pwd"))
                System.out.println(currentPath);
            else if (cmd[0].trim().equals("cd")) {
                if (cmd.length == 2)
                    cd(Paths.get(cmd[1].trim()));
                else
                    System.out.println("Expected one argument");
            } else if (cmd[0].trim().equals("mv")) {
                if (cmd.length == 3) {
                    mv(Paths.get(cmd[1]), Paths.get(cmd[2]));
                } else
                    System.out.println("Expected two argument");
            }
        }
    }

    private static boolean checkPath(Path path, String necessaryRoot, boolean finishWork) {
        boolean ok = true;
        if (!Files.exists(path)) {
            System.out.println("Path doesn't exists");
            return false;
        }
        if (necessaryRoot.contains("x") && !path.toFile().canExecute()) {
            System.out.println("No roots to execute");
            ok = false;
        }
        if (necessaryRoot.contains("r") && !path.toFile().canRead()) {
            System.out.println("No roots to read");
            ok = false;
        }
        if (necessaryRoot.contains("w") && !path.toFile().canWrite()) {
            System.out.println("No roots to write");
            ok = false;
        }
        if (!ok && finishWork)
            System.exit(-1);
        return ok;
    }

    private static Path correctPath(Path path, Path currentPath, String necessaryRoot) {
        Path tmp;
        if (path.isAbsolute()) {
            if (checkPath(path, necessaryRoot, false))
                return path;
        } else {
            tmp = Paths.get(currentPath.toString() + "/" + path.toString());
            if (checkPath(tmp, necessaryRoot, false))
                return tmp;
        }
        return currentPath;
    }

    private static void mv(Path path1, Path path2) {
        Path tmp;
        if (!path2.isAbsolute()) {
            path2 = Paths.get(currentPath.toString() + "/" + path2.toString());
        }
        try {
            Files.move(correctPath(path1, currentPath, ""), path2);
        } catch (AccessDeniedException ex) {
            System.out.println("mv: " + path1 + ": Permission denied");
        } catch (IOException ex) {
            System.out.println("mv: Error");
        }
    }

    private static void cd(Path dir) {
        Path tmp;
        if (dir.isAbsolute()) {
            if (checkPath(dir, "x", false))
                currentPath = dir;
        } else {
            tmp = Paths.get(currentPath.toString() + "/" + dir.toString());
            if (checkPath(tmp, "x", false))
                currentPath = tmp.normalize();
        }
    }

    private static void ls(Path dir) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*")) {
            for (Path entry : stream) {
                long size = Files.readAttributes(entry, BasicFileAttributes.class).size();
                String sizeMeasure = "B";
                if (size >= 1000 && size < 1000_000) {
                    sizeMeasure = "KB";
                    size /= 1000;
                }
                if (size >= 1000_000 && size < 1000_000_000) {
                    sizeMeasure = "MB";
                    size /= 1000_000;
                }
                if (size >= 1000_000_000) {
                    sizeMeasure = "GB";
                    size /= 1000_000_000;
                }
                System.out.println(entry.getFileName() + "\t" + size + sizeMeasure);
            }
        } catch (AccessDeniedException ex) {
            System.out.println("ls: " + dir.getFileName() + ": Permission denied");
        } catch (DirectoryIteratorException | IOException ex) {
            System.out.println("ls: Error");
        }
    }
}
