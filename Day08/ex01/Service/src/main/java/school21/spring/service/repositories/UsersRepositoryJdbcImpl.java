package school21.spring.service.repositories;

import school21.spring.service.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UsersRepositoryJdbcImpl implements UsersRepository{
    private DataSource dataSource;

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        String query = "SELECT * FROM users ";
        try {
            Connection connection = dataSource.getConnection();
            ResultSet userRow = connection.createStatement().executeQuery(query);
            while (userRow.next())
            {
                User user = new User().setId(userRow.getLong("id"))
                        .setEmail(userRow.getString("email").trim());
                users.add(user);
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println("Error while request " + query + ". " + e.getMessage());;
        }
        return users;
    }

    @Override
    public User findById(Long id) {
        String requestStr = "SELECT * FROM users WHERE id = " + id;
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet userRow = statement.executeQuery(requestStr);
            if (userRow.next())
            {
                User user = new User().setId(userRow.getLong("id"))
                        .setEmail(userRow.getString("email").trim());
                connection.close();
                return user;
            }
            connection.close();
            return null;
        } catch (SQLException e) {
            System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
        }
        return null;
    }

    @Override
    public void save(User user) {
        if (!Objects.nonNull(user))
            throw new NullPointerException();
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement prepStatement = connection.prepareStatement("INSERT INTO users(email) VALUES (?)");
            prepStatement.setString(1, user.getEmail());
            prepStatement.executeUpdate();
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT id FROM users WHERE email = '" + user.getEmail() + "'");
            if (!resultSet.next())
                throw new SQLException();
            user.setId(resultSet.getLong(1));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(User user) {
        if (!Objects.nonNull(user))
            throw new NullPointerException();
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement prepStatement = connection.prepareStatement("UPDATE users SET email = ? WHERE id = ?");
            prepStatement.setString(1, user.getEmail());
            prepStatement.setLong(2, user.getId());
            prepStatement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Long id) {
        String requestStr = "DELETE FROM users WHERE id = ?";
        try {
            if (findById(id) != null) {
                PreparedStatement statement = dataSource.getConnection().prepareStatement(requestStr);
                statement.setLong(1, id);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        String requestStr = "SELECT * FROM users WHERE email = " + email;
        try {
            Connection connection = dataSource.getConnection();
            ResultSet userRow = connection.createStatement().executeQuery(requestStr);
            if (userRow.next())
            {
                User user = new User().setId(userRow.getLong("id"))
                        .setEmail(userRow.getString("email").trim());
                connection.close();
                return Optional.of(user);
            }
            connection.close();
            return Optional.empty();
        } catch (SQLException e) {
            System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
        }
        return Optional.empty();
    }
}
