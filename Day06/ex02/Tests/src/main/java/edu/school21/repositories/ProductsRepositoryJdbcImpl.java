package edu.school21.repositories;

import edu.school21.models.Product;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ProductsRepositoryJdbcImpl implements ProductsRepository{
    private Connection connection;

    public ProductsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Product> findById(Long id) {
        String requestStr = "SELECT * FROM products WHERE id = " + id;
        try {
            Statement statement = connection.createStatement();
            ResultSet productRow = statement.executeQuery(requestStr);
            if (productRow.next())
            {
                Product product = new Product().setId(productRow.getLong("id"))
                        .setName(productRow.getString("name").trim())
                        .setPrice(productRow.getLong("price"));
                
                return Optional.of(product);
            }
            
            return Optional.empty();
        } catch (SQLException e) {
            System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
        }
        return Optional.empty();
    }

    @Override
    public List<Product> findAll() {
        ArrayList<Product> products = new ArrayList<>();
        String requestStr = "SELECT * FROM products ";
        try {
            Statement statement = connection.createStatement();
            ResultSet productRow = statement.executeQuery(requestStr);
            while (productRow.next())
            {
                Product user = new Product().setId(productRow.getLong("id"))
                        .setName(productRow.getString("name").trim())
                        .setPrice(productRow.getLong("price"));
                products.add(user);
            }
            
        } catch (SQLException e) {
            System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
        }
        return products;
    }

    @Override
    public void update(Product product) {
        try {
            PreparedStatement prepStatement = connection.prepareStatement("UPDATE products SET name = ?, price = ? WHERE id = ?");
            prepStatement.setString(1, product.getName());
            prepStatement.setLong(2, product.getPrice());
            prepStatement.setLong(3, product.getId());
            prepStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(Product product) {
        try {
            PreparedStatement prepStatement = connection.prepareStatement("INSERT INTO products(name, price) VALUES (?, ?)");
            prepStatement.setString(1, product.getName());
            prepStatement.setLong(2, product.getPrice());
            prepStatement.executeUpdate();
            Statement statement= connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id FROM products WHERE name = '" + product.getName() + "'");
            if (!resultSet.next())
                throw new SQLException();
            product.setId(resultSet.getLong(1));
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Long id) {
        String requestStr = "DELETE FROM products WHERE id = ?";

        try {
            if (findById(id).isPresent()) {
                PreparedStatement statement = connection.prepareStatement(requestStr);
                statement.setLong(1, id);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
