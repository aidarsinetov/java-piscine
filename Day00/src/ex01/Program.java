package ex01;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        int number = new Scanner(System.in).nextInt();
        if (number <= 1)
        {
            System.err.println("IllegalArgument");
            System.exit(-1);
        }
        boolean isPrime = true;
        int i = 2;
        for (; i <= sqrt(number); i++)
        {
            if (number % i == 0)
            {
                isPrime = false;
                break;
            }
        }
        System.out.println(isPrime + " " + (i - 1));
    }

    private static int sqrt(int num){
        double root = num / 2;
        double eps = 0.01;
        while( root - num / root > eps ){
            root = 0.5 * (root + num / root);
        }
        return (int)root;
    }
}
