package edu.school21.printer.logic;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class ImageConverter {
    private final Color whiteReplaced;
    private final Color blackReplaced;
    private InputStream input;

    public ImageConverter(Color whiteReplaced, Color blackReplaced, InputStream input) {
        this.whiteReplaced = whiteReplaced;
        this.blackReplaced = blackReplaced;
        this.input = input;
    }

    public int[][] convert() throws IOException {
        if (input == null)
            throw new NullPointerException("input == null");
        BufferedImage img = ImageIO.read(input);
        int height = img.getHeight();
        int width = img.getWidth();

        int[][] res = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int curColor =  img.getRGB(i, j);
                if (curColor == Color.BLACK.getRGB())
                    res[i][j] = blackReplaced.getRGB();
                else if (curColor == Color.WHITE.getRGB())
                    res[i][j] = whiteReplaced.getRGB();
                else
                    res[i][j] = curColor;
            }

        }

        return res;
    }
}
