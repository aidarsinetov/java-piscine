package edu.school21.chat.models;

import edu.school21.chat.app.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Objects;

public class User {
    private long id;
    private String login;
    private String password;
    private ArrayList<Chatroom> createdChrooms = new ArrayList<>();
    private ArrayList<Chatroom> includedChrooms = new ArrayList<>();

    public User() {}

    public User(String login, String password, long id) {
        this.login = login;
        this.password = password;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public User setId(long id) {
        this.id = id;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public User setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public ArrayList<Chatroom> getCreatedChatrooms() {
        return createdChrooms;
    }

    public User setCreatedChatrooms(ArrayList<Chatroom> createdChrooms) {
        this.createdChrooms = createdChrooms;
        return this;
    }

    public ArrayList<Chatroom> getIncludedChatrooms() {
        return includedChrooms;
    }

    public User setIncludedChatrooms(ArrayList<Chatroom> includedChrooms) {
        this.includedChrooms = includedChrooms;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(login, user.login) && Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwd='" + password + '\'' +
                ", createdChrooms=" + createdChrooms +
                ", includedChrooms=" + includedChrooms +
                '}';
    }
}
