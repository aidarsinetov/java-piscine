package edu.school21;

public class Car {
    private String name;
    private long serialNumber;
    private boolean isNew;
    private int speed;

    public Car() {
    }

    public Car(String name, long serialNumber, boolean isNew, int speed) {
        this.name = name;
        this.serialNumber = serialNumber;
        this.isNew = isNew;
        this.speed = speed;
    }

    public int go(int time) {
        return speed * time;
    }

    @Override
    public String
    toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", serialNumber=" + serialNumber +
                ", isNew=" + isNew +
                ", speed=" + speed +
                '}';
    }

}
