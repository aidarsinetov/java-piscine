-- SET DATABASE SQL SYNTAX PGS TRUE;

CREATE TABLE IF NOT EXISTS products (
    id  BIGINT IDENTITY PRIMARY KEY,
    name char(100) UNIQUE,
    price BIGINT
);

