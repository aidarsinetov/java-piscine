package edu.school21.spring;

public class PreProcessorToLower implements PreProcessor {
    @Override
    public String process(String msg) {
        return msg.toLowerCase();
    }
}
