package edu.school21.chat.repositories;

import edu.school21.chat.app.Program;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class ChatroomRepositoryJdbcImpl implements ChatroomRepository{
	private DataSource dataSource;

	public ChatroomRepositoryJdbcImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	@Override
	public Optional<Chatroom> findById(Long id) {
		String requestStr = "SELECT * FROM chat.rooms WHERE id = " + id;
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet userRow = statement.executeQuery(requestStr);
			if (userRow.next()){
				Chatroom chatroom = new Chatroom().setId(userRow.getLong("id"))
						.setName(userRow.getString("name").trim());
				connection.close();
				return Optional.of(chatroom);
			}
			connection.close();
			return Optional.empty();
		} catch (SQLException e) {
			System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
		}
		return Optional.empty();
	}
}
