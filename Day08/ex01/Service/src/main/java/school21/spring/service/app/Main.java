package school21.spring.service.app;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import school21.spring.service.repositories.UsersRepository;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        HikariDataSource hikariDs= context.getBean("hikariDataSource", com.zaxxer.hikari.HikariDataSource.class);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(hikariDs);
        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, email varchar(100) UNIQUE)");
        UsersRepository usersRepository = context.getBean("usersRepositoryJdbc", UsersRepository.class);
        System.out.println(usersRepository.findAll());
        usersRepository = context.getBean("usersRepositoryJdbcTemplate", UsersRepository.class);
        System.out.println(usersRepository.findAll());
        jdbcTemplate.update("DROP TABLE IF EXISTS users CASCADE");
    }
}
