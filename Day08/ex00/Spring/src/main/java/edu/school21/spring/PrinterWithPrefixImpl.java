package edu.school21.spring;

import java.util.Objects;

public class PrinterWithPrefixImpl implements Printer{
    private String prefix;
    private Renderer renderer;

    public PrinterWithPrefixImpl(Renderer renderer) {
        if (!Objects.nonNull(renderer))
            throw new NullPointerException();
        this.renderer = renderer;
    }

    public PrinterWithPrefixImpl(String prefix, Renderer renderer) {
        if (!Objects.nonNull(renderer))
            throw new NullPointerException();
        this.prefix = prefix;
        this.renderer = renderer;
    }

    @Override
    public void print(String str) {
        renderer.render(prefix + str);
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }
}
