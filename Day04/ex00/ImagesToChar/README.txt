# Deleting generated files
if [ -d target ] ; then
    rm -rf target/
fi

mkdir -p target

# Making jar archive with project in folder target/
javac -d target $(find src -name "*.java")

# Run
java -classpath target edu.school21.printer.app.Main . 0 /Users/tphlogistestbs/Documents/java-piscine/Day04/ex00/it.bmp
