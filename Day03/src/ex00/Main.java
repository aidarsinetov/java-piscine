package ex00;

class PrinterThread extends Thread {
    private int number;
    private String output;

    public PrinterThread(int number, String str) {
        this.number = number;
        this.output = str;
    }

    @Override
    public void run() {
        for (int i = 0; i < number; i++)
            System.out.println(output);
    }
}

public class Main {
    public static void main(String[] args) {
        if (args.length < 1)
            return;
        if (!args[0].startsWith("--count="))
            return;
        final int counter = Integer.parseInt(args[0].substring(args[0].indexOf('=') + 1));

        Thread eggThread = new PrinterThread(counter, "Egg");
        Thread humanThread = new PrinterThread(counter, "Hen");

        eggThread.start();
        humanThread.start();

        try {
            eggThread.join();
            humanThread.join();
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }

        for (int i = 0; i < counter; i++)
            System.out.println("Human");

    }

}
