package edu.school21.numbers;

public class NumberWorker {

    public boolean isPrime(int number) {
        if (number <= 1)
            throw new IllegalNumberException("can't execute isPrime() method for number " + number);
        boolean isPrime = true;
        int i = 2;
        for (; i <= Math.sqrt(number); i++)
        {
            if (number % i == 0)
            {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }

    public int digitsSum(int number) {
        int n = 0;
        if (number < 0)
            number = Math.abs(number);
        do {
            n += number % 10;
            number /= 10;
        } while (number / 10 >= 1);
        n += number;
        return n;
    }
}
