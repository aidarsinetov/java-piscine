package edu.school21.chat.models;

import java.util.ArrayList;
import java.util.Objects;

public class Chatroom {
    private long id;
    private String name;
    private User owner;
    private ArrayList<Message> massages;

    public Chatroom() {
    }

    public Chatroom(long id, String name, User owner) {
        if (name.isEmpty() || name.trim().isEmpty())
            throw new IllegalArgumentException("Was given empty string");
        this.id = id;
        this.name = name;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public Chatroom setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Chatroom setName(String name) {
        this.name = name;
        return this;
    }

    public User getOwner() {
        return owner;
    }

    public Chatroom setOwner(User owner) {
        this.owner = owner;
        return this;
    }

    public ArrayList<Message> getMassages() {
        return massages;
    }

    public Chatroom setMassages(ArrayList<Message> massages) {
        this.massages = massages;
        return this;
    }

    @Override
    public String toString() {
        return "Chatroom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", roomCreator=" + owner +
                ", massages=" + massages +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chatroom chatroom = (Chatroom) o;
        return id == chatroom.id && Objects.equals(name, chatroom.name) && Objects.equals(owner, chatroom.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, owner);
    }
}
