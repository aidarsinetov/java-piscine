package ex01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


public class Main {

    public static void main(String[] args) {
        if (args.length < 1)
            return;
        if (!args[0].startsWith("--count="))
            return;
        final int counter = Integer.parseInt(args[0].substring(args[0].indexOf('=') + 1));
        AtomicInteger index = new AtomicInteger();
        index.set(0);

        AtomicBoolean bool = new AtomicBoolean();
        bool.set(false);

        Thread first = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < counter; i++) {
                    if (bool.get()) {
                        System.out.println("Egg");
                        this.notify();
                    }
                    try {
                        Thread.currentThread().wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Thread second = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < counter; i++)
                    try {
                        Thread.currentThread().wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Hen");
            }
        });

        first.start();
        second.start();

    }


}
