package edu.school21.spring;

public class RendererErrImpl implements Renderer{
    private PreProcessor processor;

    public RendererErrImpl(PreProcessor processor) {
        this.processor = processor;
    }

    @Override
    public void render(String msg) {
        System.err.println(processor.process(msg));
    }
}
