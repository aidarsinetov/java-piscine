package edu.school21.sockets.server;

import edu.school21.sockets.app.Main;
import edu.school21.sockets.models.User;
import edu.school21.sockets.repositories.UsersRepository;
import edu.school21.sockets.services.UsersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    private static Server INSTANCE = null;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private final static PasswordEncoder encoder = new BCryptPasswordEncoder();


    private Server(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Server getInstance(int port) {
        if (INSTANCE == null)
            INSTANCE = new Server(port);
        return INSTANCE;
    }

    public static Server getInstance() {
        return INSTANCE;
    }


    public void start() {
        try {
            while (true){
                new ClientThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    static class  ClientThread extends Thread {
        private final Socket client;
        private final Scanner scanner;
        private final BufferedWriter writer;
        private static final UsersServiceImpl usersService = Main.CONTEXT.getBean("usersServiceImpl", UsersServiceImpl.class);
        private static final UsersRepository usersRepository = Main.CONTEXT.getBean("usersRepositoryImpl", UsersRepository.class);

        public ClientThread(Socket client) throws IOException {
            this.client = client;
            writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            scanner = new Scanner(client.getInputStream());
        }

        @Override
        public void run() {
            try {
                writer.append("Hello from Server!\n").flush();
                String line;
                while (client.getKeepAlive()) {
                    line = scanner.nextLine();
                    System.out.println(line);
                    if (line.equals("signUp")) {
                        signUp();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private synchronized void signUp() throws IOException {
            String line;
            User user = new User();
            do {
                writer.append("Enter username:\n").flush();
                line = scanner.nextLine();
                System.out.println(usersRepository);
                if (usersRepository.findByEmail(line).isPresent()) {
                    writer.append("this username is busy\n").flush();
                    line = "";
                }
            } while (line.isEmpty());
            user.setEmail(line);

            do {
                writer.append("Enter password:\n").flush();
                line = scanner.nextLine();
                if (!line.isEmpty())
                {
                    if (UsersServiceImpl.checkPassword(line))
                    {
                        user.setPassword(encoder.encode(line));
                        break;
                    } else {
                        writer.append("forbidden symbols in password\n");
                        writer.append("available symbols: " + usersService.getAvailableSymbols() + "\n");
                        writer.flush();
                    }
                }
            } while (line.isEmpty());
            usersService.signUp(user);
            writer.append("Successful!\n").flush();
            client.close();
        }
    }

}
