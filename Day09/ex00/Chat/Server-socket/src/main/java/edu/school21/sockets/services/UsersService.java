package edu.school21.sockets.services;

import edu.school21.sockets.models.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Component
public interface UsersService {
    String signUp(User user);
}
