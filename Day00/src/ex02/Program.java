package ex02;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int res = 0;
        while (true){
            double number = scanner.nextDouble();
            if (number == 42.0)
                break;
            int n = 0;
            do {
                n += number % 10;
                number /= 10;
            } while (number / 10 >= 1);
            n += number;
            if (isPrime(n))
                res++;
        }
        System.out.println("Count of coffee - request - " + res);
    }

    private static int sqrt(int num){
        double root = num / 2;
        double eps = 0.01;
        while( root - num / root > eps ){
            root = 0.5 * (root + num / root);
        }
        return (int)root;
    }

    private static boolean isPrime(int number){
        boolean isPrime = true;
        if (number <= 1)
        {
            return false;
        }
        double i = 2;
        for (; i <= sqrt(number); i++)
        {
            if (number % i == 0)
            {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }
}
