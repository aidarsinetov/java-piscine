package edu.school21.chat.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.school21.chat.models.Message;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.NOPLogger;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Scanner;

public class Program {
	public static final String DB_URL = "jdbc:postgresql://localhost/postgres";
	public static final String DB_USER = "postgres";
	public static final String DB_PWD = "postgres";
	private static final String DB_SCHEMA = "schema.sql";
	private static final String DB_DATA = "data.sql";
	private static final Logger logger = LoggerFactory.getLogger(Program.class);

	public static void main(String[] args) throws SQLException{
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			logger.info("");
			e.printStackTrace();
		}
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(DB_URL);
		config.setUsername(DB_USER);
		config.setPassword(DB_PWD);
		DataSource dataSource = new HikariDataSource(config);
		Connection connection = dataSource.getConnection();
		execSQLFile(connection, ClassLoader.getSystemResourceAsStream(DB_SCHEMA));
		execSQLFile(connection, ClassLoader.getSystemResourceAsStream(DB_DATA));
		connection.close();
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNextLong())
		{
			long id = scanner.nextLong();
			MessagesRepositoryJdbcImpl msgRep = new MessagesRepositoryJdbcImpl(dataSource);
			Optional<Message> msg = msgRep.findById(id);
			if (msg.isPresent())
				System.out.println(msg.get());
			else
				System.out.println("Message with id=" + id + " didn't find in database!!");
		}
	}

	private static void execSQLFile(Connection con, InputStream in) throws SQLException {
		Scanner sc = new Scanner(in).useDelimiter(";");
		Statement statement = con.createStatement();
		while (sc.hasNext()) {
			String st = sc.next().replaceAll("\n", "");
			System.out.println(st + ";");
			statement.execute(st + ";");
		}

	}
}
