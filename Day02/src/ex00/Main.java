import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private final static String FILE_TO_SAVE = "result.txt";
    private final static String SIGNATURES_FILE = "signatures.txt";

    public static void main(String[] args) throws IOException {
        HashMap<String, String> signatures = readSignatures();
        FileOutputStream fos = new FileOutputStream(FILE_TO_SAVE);
        int maxByteLength = signatures.values().stream().map(String::length).max(Integer::compare).get();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine())
        {
            String fileName = scanner.nextLine();
            if (fileName.trim().equals("42"))
                break;
            try {
                FileInputStream fis = new FileInputStream(fileName);
                final String finalHeader = readFileHeader(maxByteLength, fis);
                fis.close();
                Optional<String> fileHeader = signatures.values().stream().filter(x -> finalHeader.startsWith(x)).findFirst();
                if (!fileHeader.isPresent())
                    System.out.println("UNDEFINED");
                else {
                    System.out.println("PROCESSED");
                    int index = new ArrayList<>(signatures.values()).indexOf(fileHeader.get());
                    fos.write(signatures.keySet().stream().limit(index + 1).skip(index).findFirst().get().getBytes(StandardCharsets.UTF_8));
                    fos.write('\n');
                }
            } catch (IOException ex) {
                System.out.println("Error");
            }
        }
        fos.close();
    }

    private static String readFileHeader(int maxByteLength, FileInputStream targetFIS) throws IOException {
        int[] result = new int[maxByteLength];
        for (int i = 0; i < maxByteLength && targetFIS.available() > 0; i++)
            result[i] = targetFIS.read();
        return Arrays.stream(result).mapToObj(Integer::toHexString).collect(Collectors.joining());
    }


    private static HashMap<String, String> readSignatures() throws IOException {
        FileInputStream fis = new FileInputStream(SIGNATURES_FILE);
        HashMap<String, String> signatures = new HashMap<>();
        String line = "", k = "", v = "";
        int i;
        while((i = fis.read()) != -1) {
            if ((char)i == '\n')
            {
                k = line.substring(0, line.indexOf(','));
                v = line.substring(line.indexOf(',') + 1).toLowerCase().replace(" 0", "").replace(" ", "");
                signatures.put(k, v);
                line = "";
                continue ;
            }
            line += (char)i;
        }
        fis.close();
        return signatures;
    }
}
