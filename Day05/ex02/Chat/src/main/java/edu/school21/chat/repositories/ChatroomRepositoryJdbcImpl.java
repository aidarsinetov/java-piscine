package edu.school21.chat.repositories;

import edu.school21.chat.exceptions.NotSavedSubEntityException;
import edu.school21.chat.models.Chatroom;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Objects;
import java.util.Optional;

public class ChatroomRepositoryJdbcImpl implements ChatroomRepository{
	private DataSource dataSource;

	public ChatroomRepositoryJdbcImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	@Override
	public Optional<Chatroom> findById(Long id) {
		String requestStr = "SELECT * FROM chat.rooms WHERE id = " + id;
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet userRow = statement.executeQuery(requestStr);
			if (userRow.next()){
				Chatroom chatroom = new Chatroom().setId(userRow.getLong("id"))
						.setName(userRow.getString("name").trim());
				connection.close();
				return Optional.of(chatroom);
			}
			connection.close();
			return Optional.empty();
		} catch (SQLException e) {
			System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
		}
		return Optional.empty();
	}

	@Override
	public boolean save(Chatroom room) {
		if (!Objects.nonNull(room) || !Objects.nonNull(room.getName()) || !Objects.nonNull(room.getOwner()))
			throw new NullPointerException();
		try {
			Connection connection = dataSource.getConnection();

			UserRepositoryJdbcImpl userRepos = new UserRepositoryJdbcImpl(dataSource);
			if (!userRepos.findById(room.getOwner().getId()).isPresent())
			{
				connection.close();
				throw new NotSavedSubEntityException("Room owner (Chatroom.owner field) wasn't find in database ");
			}

			PreparedStatement statement = connection.prepareStatement("INSERT INTO chat.rooms(name, owner) VALUES (?, ?)");
			statement.setString(1, room.getName());
			statement.setLong(2, room.getOwner().getId());
			statement.executeUpdate();

			statement = connection.prepareStatement("SELECT id FROM chat.rooms WHERE name = ? AND owner = ?");
			statement.setString(1, room.getName());
			statement.setLong(2, room.getOwner().getId());
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next())
				throw new SQLException();
			room.setId(resultSet.getLong(1));
			connection.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());;
			return false;
		}
	}
}
