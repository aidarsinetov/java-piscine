package edu.school21.chat.models;

import java.util.ArrayList;
import java.util.Objects;

public class Chatroom {
    private long id;
    private String name;
    private User roomCreator;
    private ArrayList<Message> massages;

    public Chatroom() {
    }

    public Chatroom(long id, String name, User roomCreator) {
        this.id = id;
        this.name = name;
        this.roomCreator = roomCreator;
    }

    public long getId() {
        return id;
    }

    public Chatroom setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Chatroom setName(String name) {
        this.name = name;
        return this;
    }

    public User getRoomCreator() {
        return roomCreator;
    }

    public Chatroom setRoomCreator(User roomCreator) {
        this.roomCreator = roomCreator;
        return this;
    }

    public ArrayList<Message> getMassages() {
        return massages;
    }

    public Chatroom setMassages(ArrayList<Message> massages) {
        this.massages = massages;
        return this;
    }

    @Override
    public String toString() {
        return "Chatroom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", roomCreator=" + roomCreator +
                ", massages=" + massages +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chatroom chatroom = (Chatroom) o;
        return id == chatroom.id && Objects.equals(name, chatroom.name) && Objects.equals(roomCreator, chatroom.roomCreator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, roomCreator);
    }
}
