package school21.spring.service.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import school21.spring.service.models.User;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate template;

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        template = new JdbcTemplate(dataSource);
    }

    @Override
    public User findById(Long id) {
        return template.queryForObject("SELECT * FROM users WHERE id = " + id,
                (rs, rowNum) -> new User(id, rs.getString("email"), rs.getString("password")));
    }

    @Override
    public List<User> findAll() {
        return template.query("SELECT * FROM users",
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("email"), rs.getString("password")));
    }

    @Override
    public void save(User entity) {
        template.update("INSERT INTO users(email, password) VALUES ('" + entity.getEmail() + "', '" + entity.getPassword() +"')");
    }

    @Override
    public void update(User user) {
        template.update("UPDATE users SET email = '" + user.getEmail() +"', password = '" + user.getPassword() + "' WHERE id = " + user.getId());
    }

    @Override
    public void delete(Long id) {
        template.update("DELETE FROM users WHERE id = " + id);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(template.queryForObject("SELECT * FROM users WHERE email = '" + email + "'",
                (rs, rowNum) -> new User(rs.getLong("id"), email, rs.getString("password"))));
    }
}
