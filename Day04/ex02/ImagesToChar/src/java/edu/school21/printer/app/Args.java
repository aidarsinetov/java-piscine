package edu.school21.printer.app;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.awt.*;

class ColorConverter implements IStringConverter<Color> {
    @Override
    public Color convert(String value) {
        switch (value) {
            case "RED":
                return Color.RED;
            case "GREEN":
                return Color.GREEN;
            case "BLUE":
                return Color.BLUE;
            case "BLACK":
                return Color.BLACK;
            case "WHITE":
                return Color.WHITE;
            case "YELLOW":
                return Color.YELLOW;
            case "GRAY":
                return Color.GRAY;
            case "PINK":
                return Color.PINK;
            case "ORANGE":
                return Color.ORANGE;
            case "MAGENTA":
                return Color.MAGENTA;
            default:
                System.out.println("Error: unknown color!");
                System.out.println("Available colors: RED, GREEN, BLUE, BLACK, WHITE, YELLOW, GRAY, PINK, ORANGE, MAGENTA.");
                System.exit(-1);
                return null;
        }
    }
}

@Parameters(separators = "=")
public class Args {
    @Parameter(names = "--white", description = "Color on which will be replaced white color", converter = ColorConverter.class)
    private static Color whiteReplaced = Color.WHITE;

    @Parameter(names = "--black", description = "Color on which will be replaced black color", converter = ColorConverter.class)
    private static Color blackReplaced = Color.BLACK;

    public static Color getWhiteReplaced() {
        return whiteReplaced;
    }

    public static Color getBlackReplaced() {
        return blackReplaced;
    }
}

