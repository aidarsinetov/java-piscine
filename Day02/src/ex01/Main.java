import java.io.*;
import java.util.*;
import java.util.stream.*;

public class Main {
    private final static String FILE_TO_SAVE = "Dictionary.txt";

    public static void main(String[] args) throws IOException {
        if (args.length != 2)
        {
            System.out.println("Expected two arguments");
            return;
        }
        BufferedReader first = new BufferedReader(new FileReader(args[0]));
        BufferedReader second = new BufferedReader(new FileReader(args[1]));

        File firstFile = File.readFile(first);
        File secondFile = File.readFile(second);

        ArrayList<String> overallVocab = new ArrayList<>(Stream.concat(firstFile.vocab.stream(), secondFile.vocab.stream()).distinct().sorted().collect(Collectors.toList()));

        firstFile.fillCounters(overallVocab);
        secondFile.fillCounters(overallVocab);

        saveToFile(firstFile, secondFile, overallVocab);
        System.out.println("Similarity = " + calculateSimilarity(firstFile.counters, secondFile.counters));
    }

    private static void saveToFile(File firstFile, File secondFile, ArrayList<String> overallVocab) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_TO_SAVE));
        int maxLength = overallVocab.stream().map(String::length).max(Integer::compare).get() + 10;
        for (int i = 0; i < overallVocab.size(); i++) {
            writer.append(Integer.toString(i + 1));
            writer.append('\t');
            writer.append(overallVocab.get(i));
            writer.append(Collections.nCopies(maxLength - overallVocab.get(i).length(), " ").stream().reduce((a, b) ->a + b).get());
            writer.append(Integer.toString(firstFile.counters.get(i)));
            writer.append("\t\t");
            writer.append(Integer.toString(secondFile.counters.get(i)));
            writer.append('\n');
        }
        writer.flush();
        writer.close();
    }

    private static double calculateSimilarity(Vector<Integer> firstCounters, Vector<Integer> secondCounters){
        double numerator = 0, firstDenominator = 0, secondDenominator = 0;
        for (int i = 0; i < firstCounters.size(); i++) {
            numerator += firstCounters.get(i) * secondCounters.get(i);
            firstDenominator += firstCounters.get(i) * firstCounters.get(i);
            secondDenominator += secondCounters.get(i) * secondCounters.get(i);
        }
        return numerator / (Math.sqrt(firstDenominator) * Math.sqrt(secondDenominator));
    }

    private static class File{
        private ArrayList<String> vocab;
        private Vector<Integer> counters;
        private ArrayList<String> words;

        private static File readFile(BufferedReader reader) throws IOException {
            ArrayList<String> words = new ArrayList<>(reader.lines().flatMap(x -> Arrays.stream(x.split(" "))).collect(Collectors.toList()));
            ArrayList<String> vocab = new ArrayList<>(words.stream().distinct().sorted().collect(Collectors.toList()));
            reader.close();
            return new File().setVocab(vocab).setWords(words);
        }

        public File fillCounters(ArrayList<String> overallVocab){
            counters = new Vector<>(overallVocab.size());
            for (int i = 0; i < overallVocab.size(); i++) {
                int index = i;
                counters.add(i, (int) words.stream().filter(x->overallVocab.get(index).equals(x)).count());
            }
            return this;
        }

        public Vector<Integer> getCounters() {
            return counters;
        }

        public File setCounters(Vector<Integer> counters) {
            this.counters = counters;
            return this;
        }

        public ArrayList<String> getVocab() {
            return vocab;
        }

        public File setVocab(ArrayList<String> vocab) {
            this.vocab = vocab;
            return this;
        }

        public ArrayList<String> getWords() {
            return words;
        }

        public File setWords(ArrayList<String> words) {
            this.words = words;
            return this;
        }
    }

}
