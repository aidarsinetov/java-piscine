package edu.school21.printer.logic;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class ImageConverter {
    private final char white;
    private final char black;
    private InputStream input;

    public ImageConverter(char white, char black, InputStream input) {
        this.white = white;
        this.black = black;
        this.input = input;
    }

    public int[][] convert() throws IOException {
        BufferedImage img = ImageIO.read(input);
        int height = img.getHeight();
        int width = img.getWidth();

        int[][] res = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                res[i][j] = img.getRGB(i, j) == Color.BLACK.getRGB() ? black : white;
            }
        }
        return res;
    }
}
