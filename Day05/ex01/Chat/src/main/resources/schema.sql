DROP SCHEMA IF EXISTS chat CASCADE;

CREATE SCHEMA IF NOT EXISTS chat;

CREATE TABLE IF NOT EXISTS chat.users(
	id				BIGSERIAL PRIMARY KEY UNIQUE,
	login			char(30) UNIQUE NOT NULL,
	password		char(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS chat.rooms(
	id		BIGSERIAL PRIMARY KEY UNIQUE,
	name	char(100) NOT NULL UNIQUE,
	owner int REFERENCES chat.users(id)
);
CREATE TABLE IF NOT EXISTS chat.messages(
	id			BIGSERIAL PRIMARY KEY UNIQUE,
	text		text NOT NULL,
	date		date NOT NULL,
	time		time NOT NULL,
	author		int REFERENCES chat.users(id),
	chatroom	int REFERENCES chat.rooms(id)
);

CREATE TABLE IF NOT EXISTS chat.users_in_chats(
  room_id	int REFERENCES chat.rooms(id),
  user_id	int REFERENCES chat.users(id),
  PRIMARY KEY (room_id, user_id)
);


