INSERT INTO chat.users(login, password) VALUES ('Thor', '498ouiklj-=-ph');
INSERT INTO chat.users(login, password) VALUES ('Loki', '0ou-u8');
INSERT INTO chat.users(login, password) VALUES ('Odin', '435tyt1');
INSERT INTO chat.users(login, password) VALUES ('Hela', '32rrw5yty');
INSERT INTO chat.users(login, password) VALUES ('Heimdall', '897adfsf67');

INSERT INTO chat.rooms(name, owner) VALUES ('Thor''s club', 1);
INSERT INTO chat.rooms(name, owner) VALUES ('Asgard forever', 3);
INSERT INTO chat.rooms(name, owner) VALUES ('villains', 4);
INSERT INTO chat.rooms(name, owner) VALUES ('Asgard news', 5);
INSERT INTO chat.rooms(name, owner) VALUES ('illegitimate children', 4);

INSERT INTO chat.users_in_chats(room_id, user_id) VALUES (1, 1);
INSERT INTO chat.users_in_chats(room_id, user_id) VALUES (2, 5);
INSERT INTO chat.users_in_chats(room_id, user_id) VALUES (4, 5);
INSERT INTO chat.users_in_chats(room_id, user_id) VALUES (4, 3);
INSERT INTO chat.users_in_chats(room_id, user_id) VALUES (4, 4);

INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES ('Loki is crud', '10/11/2013', '10:15', 1, 1);
INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES ('Iiyghkj', current_date, current_time, 1, 1);
INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES ('Asgard forever msg', current_date, current_time, 5, 2);
INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES ('Heimdall jklio', current_date, current_time, 5, 4);
INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES ('OPHOI IOYHPU', current_date, current_time, 3, 4);
INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES ('bad news', current_date, current_time, 4, 4);