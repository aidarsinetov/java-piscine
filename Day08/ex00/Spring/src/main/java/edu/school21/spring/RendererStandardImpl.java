package edu.school21.spring;

import java.util.Objects;

public class RendererStandardImpl implements Renderer {
    private PreProcessor processor;

    public RendererStandardImpl (PreProcessor processor) {
        if (!Objects.nonNull(processor))
            throw new NullPointerException();
        this.processor = processor;
    }

    @Override
    public void render(String msg) {
        System.out.println(processor.process(msg));
    }
}
