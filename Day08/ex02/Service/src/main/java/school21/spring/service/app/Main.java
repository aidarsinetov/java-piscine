package school21.spring.service.app;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import school21.spring.service.config.ApplicationConfig;
import school21.spring.service.repositories.UsersRepository;

import javax.sql.DataSource;

public class Main {
    public static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ApplicationConfig.class);

    static {
        DataSource dataSource = Main.CONTEXT.getBean(ApplicationConfig.class).dataSource();
        JdbcTemplate template = new JdbcTemplate(dataSource);
        template.update("CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, email varchar(100) NOT NUll UNIQUE, password varchar(100))");
    }

    public static void main(String[] args) {
        UsersRepository usersRepository = CONTEXT.getBean("usersRepositoryJdbc", UsersRepository.class);
        System.out.println(usersRepository.findAll());
        usersRepository = CONTEXT.getBean("usersRepositoryJdbcTemplate", UsersRepository.class);
        System.out.println(usersRepository.findAll());
    }
}
