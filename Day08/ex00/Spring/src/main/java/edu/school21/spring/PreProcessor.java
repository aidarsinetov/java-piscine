package edu.school21.spring;

public interface PreProcessor {
    String process(String msg);
}
