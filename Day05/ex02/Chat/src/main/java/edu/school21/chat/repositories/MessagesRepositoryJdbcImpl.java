package edu.school21.chat.repositories;

import edu.school21.chat.exceptions.NotSavedSubEntityException;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Objects;
import java.util.Optional;

public class MessagesRepositoryJdbcImpl implements MessagesRepository{
	private DataSource dataSource;

	public MessagesRepositoryJdbcImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Optional<Message> findById(Long id) {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet msgRow = statement.executeQuery("SELECT * FROM chat.messages WHERE id = " + id);
			if (msgRow.next()) {
				Message message = new Message().setId(msgRow.getLong("id")).setText(msgRow.getString("text"));
				User author = new UserRepositoryJdbcImpl(dataSource).findById(msgRow.getLong("author")).orElse(null);
				message.setAuthor(author);
				Chatroom room = new ChatroomRepositoryJdbcImpl(dataSource).findById(msgRow.getLong("chatroom")).orElse(null);
				message.setChatroom(room);
				message.setDate(msgRow.getDate("date")).setTime(msgRow.getTime("time"));
				connection.close();
				return Optional.of(message);
			}
			connection.close();
			return Optional.empty();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	@Override
	public boolean save(Message message) throws NotSavedSubEntityException {
		if (!Objects.nonNull(message))
			throw new NullPointerException();
		if (!Objects.nonNull(message.getAuthor()) || !Objects.nonNull(message.getChatroom()))
			throw new NotSavedSubEntityException("Message has to have non null 'author' and 'chatroom' fields");
		try {
			Connection connection = dataSource.getConnection();

			checkExists(message, connection);
			PreparedStatement statement = connection.prepareStatement("INSERT INTO chat.messages(text, date, time, author, chatroom) VALUES (?, ?, ?, ?, ?)");
			statement.setString(1, message.getText());
			statement.setDate(2, message.getDate());
			statement.setTime(3, message.getTime());
			statement.setLong(4, message.getAuthor().getId());
			statement.setLong(5, message.getChatroom().getId());
			statement.executeUpdate();

			statement = connection.prepareStatement("SELECT id FROM chat.messages WHERE text = ? AND date = ? AND time = ? AND author = ?");
			statement.setString(1, message.getText());
			statement.setDate(2, message.getDate());
			statement.setTime(3, message.getTime());
			statement.setLong(4, message.getAuthor().getId());
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next())
				throw new SQLException();
			message.setId(resultSet.getLong(1));
			connection.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	private void checkExists(Message message, Connection connection) throws SQLException {
		UserRepositoryJdbcImpl userRepos = new UserRepositoryJdbcImpl(dataSource);
		if (!userRepos.findById(message.getAuthor().getId()).isPresent())
		{
			connection.close();
			throw new NotSavedSubEntityException("User (Message.author field) wasn't find in database");
		}
		ChatroomRepository chatroomRepository = new ChatroomRepositoryJdbcImpl(dataSource);
		if (!chatroomRepository.findById(message.getChatroom().getId()).isPresent())
		{
			connection.close();
			throw new NotSavedSubEntityException("Chatroom (Message.chatroom field) wasn't find in database ");
		}
	}
}
