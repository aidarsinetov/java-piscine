package school21.spring.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import school21.spring.service.models.User;
import school21.spring.service.repositories.UsersRepository;
import java.sql.SQLException;
import java.util.Random;

@Component
public class UsersServiceImpl implements UsersService {
    @Autowired
    @Qualifier("usersRepositoryJdbcTemplate")
    UsersRepository usersRepository;

    @Override
    public String signUp(String email) throws SQLException {
        User user = usersRepository.findByEmail(email).orElse(new User().setEmail(email));
        if (user.getId() == null) {
            usersRepository.save(user);
        } else return user.getPassword();
        String password = generatePassword();
        user.setPassword(password);
        usersRepository.save(user);
        return password;
    }

    private String generatePassword() {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-#*@";
        Random rnd = new Random();

        StringBuilder sb = new StringBuilder(9 + rnd.nextInt(11));
        for (int i = 0; i < sb.capacity(); i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
}
