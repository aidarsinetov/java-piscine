package school21.spring.service.repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import school21.spring.service.models.User;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {
    private DataSource dataSource;
    private JdbcTemplate template;

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        template = new JdbcTemplate(dataSource);
    }

    @Override
    public User findById(Long id) {
        return template.queryForObject("SELECT * FROM users WHERE id = " + id,
                (rs, rowNum) -> new User(id, rs.getString("email")));
    }

    @Override
    public List<User> findAll() {
        return template.query("SELECT * FROM users",
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("email")));
    }

    @Override
    public void save(User entity) {
        template.update("INSERT INTO users(email) VALUES ('" + entity.getEmail() + "')");
    }

    @Override
    public void update(User entity) {
        template.update("UPDATE users SET email = '" + entity.getEmail() +"' WHERE id = " + entity.getId());
    }

    @Override
    public void delete(Long id) {
        template.update("DELETE FROM users WHERE id = " + id);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.of(template.queryForObject("SELECT * FROM users WHERE email = '" + email + "'",
                (rs, rowNum) -> new User(rs.getLong("id"), email)));
    }
}
