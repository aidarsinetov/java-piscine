package edu.school21.chat.repositories;

import edu.school21.chat.models.User;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UserRepositoryJdbcImpl implements UserRepository{
	private DataSource dataSource;

	public UserRepositoryJdbcImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<User> findAll(int page, int size) {
		ArrayList<User> users = new ArrayList<>();
		if (page <= 0 || size <= 0)
			return users;
		String requestStr = "SELECT * FROM chat.users LIMIT ? OFFSET ?";
		try {
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(requestStr);
			statement.setInt(1, size);
			statement.setInt(2, (page - 1) * size);
			ResultSet userRow = statement.executeQuery();
			while (userRow.next())
			{
				User user = new User().setId(userRow.getLong("id"))
						.setLogin(userRow.getString("login").trim())
						.setPassword(userRow.getString("password").trim());
				users.add(user);
			}
			connection.close();
		} catch (SQLException e) {
			System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
		}
		return users;
	}

	@Override
	public Optional<User> findById(Long id) {
		String requestStr = "SELECT * FROM chat.users WHERE id = " + id;
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet userRow = statement.executeQuery(requestStr);
			if (userRow.next())
			{
				User user = new User().setId(userRow.getLong("id"))
						.setLogin(userRow.getString("login").trim())
						.setPassword(userRow.getString("password").trim());
				connection.close();
				return Optional.of(user);
			}
			connection.close();
			return Optional.empty();
		} catch (SQLException e) {
			System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
		}
		return Optional.empty();
	}

	@Override
	public boolean save(User user) {
		if (!Objects.nonNull(user) || !Objects.nonNull(user.getLogin()) || !Objects.nonNull(user.getPassword()))
			throw new NullPointerException();
		try {
			Connection connection = dataSource.getConnection();
			PreparedStatement prepStatement = connection.prepareStatement("INSERT INTO chat.users(login, password) VALUES (?, ?)");
			prepStatement.setString(1, user.getLogin());
			prepStatement.setString(2, user.getPassword());
			prepStatement.executeUpdate();
			Statement statement= connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT id FROM chat.users WHERE login = '" + user.getLogin() + "'");
			if (!resultSet.next())
				throw new SQLException();
			user.setId(resultSet.getLong(1));
			connection.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(User user) {
		if (!Objects.nonNull(user))
			throw new NullPointerException();
		try {
			Connection connection = dataSource.getConnection();
			PreparedStatement prepStatement = connection.prepareStatement("UPDATE chat.users SET login = ?, password = ? WHERE id = ?");
			prepStatement.setString(1, user.getLogin());
			prepStatement.setString(2, user.getPassword());
			prepStatement.setLong(3, user.getId());
			prepStatement.executeUpdate();
			connection.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
