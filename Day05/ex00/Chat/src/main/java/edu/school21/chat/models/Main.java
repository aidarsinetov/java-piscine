package edu.school21.chat.models;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
	private static final String DB_URL = "jdbc:postgresql://localhost/postgres";
	private static final String DB_USER = "postgres";
	private static final String DB_PWD = "postgres";
	private static final String DB_SCHEMA = "schema.sql";
	private static final String DB_DATA = "data.sql";

	public static void main(String[] args) throws SQLException{
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PWD);
		execSQLFile(connection, ClassLoader.getSystemResourceAsStream(DB_SCHEMA));
		execSQLFile(connection, ClassLoader.getSystemResourceAsStream(DB_DATA));
	}

	private static void execSQLFile(Connection con, InputStream in) throws SQLException {
		Scanner sc = new Scanner(in).useDelimiter(";");
		Statement statement = con.createStatement();
		while (sc.hasNext()) {
			String st = sc.next().replaceAll("\n", "");
			System.out.println(st + ";");
			statement.execute(st + ";");
		}

	}
}
