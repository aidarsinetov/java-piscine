package edu.school21.chat.repositories;

import edu.school21.chat.app.Program;
import edu.school21.chat.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class UserRepositoryJdbcImpl implements UserRepository{
	private DataSource dataSource;

	public UserRepositoryJdbcImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Optional<User> findById(Long id) {
		String requestStr = "SELECT * FROM chat.users WHERE id = " + id;
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet userRow = statement.executeQuery(requestStr);
			if (userRow.next())
			{
				User user = new User().setId(userRow.getLong("id"))
						.setLogin(userRow.getString("login").trim())
						.setPassword(userRow.getString("password").trim());
				connection.close();
				return Optional.of(user);
			}
			connection.close();
			return Optional.empty();
		} catch (SQLException e) {
			System.out.println("Error while request " + requestStr + ". " + e.getMessage());;
		}
		return Optional.empty();
	}
}
