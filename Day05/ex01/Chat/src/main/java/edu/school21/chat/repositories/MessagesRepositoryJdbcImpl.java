package edu.school21.chat.repositories;

import edu.school21.chat.app.Program;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class MessagesRepositoryJdbcImpl implements MessagesRepository{
	private DataSource dataSource;

	public MessagesRepositoryJdbcImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Optional<Message> findById(Long id) {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet msgRow = statement.executeQuery("SELECT * FROM chat.messages WHERE id = " + id);
			if (msgRow.next()) {
				Message message = new Message().setId(msgRow.getLong("id")).setText(msgRow.getString("text"));
				User author = new UserRepositoryJdbcImpl(dataSource).findById(msgRow.getLong("author")).orElse(null);
				message.setAuthor(author);
				Chatroom room = new ChatroomRepositoryJdbcImpl(dataSource).findById(msgRow.getLong("chatroom")).orElse(null);
				message.setChatroom(room);
				message.setDate(msgRow.getDate("date")).setTime(msgRow.getTime("time"));
				connection.close();
				return Optional.of(message);
			}
			connection.close();
			return Optional.empty();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
}
