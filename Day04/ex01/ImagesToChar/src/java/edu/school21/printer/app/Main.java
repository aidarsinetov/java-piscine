package edu.school21.printer.app;

import edu.school21.printer.logic.ImageConverter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        if (args.length != 2)
        {
            System.out.println("Expected two arguments");
            System.exit(-1);
        }

        final char white = args[0].charAt(0);
        final char black = args[1].charAt(0);

        try {
            int[][] img = new ImageConverter(white, black, ClassLoader.getSystemResourceAsStream("resources/it.bmp")).convert();
            for (int i = 0; i < img.length; i++)
            {
                for (int j = 0; j < img[0].length; j++)
                    System.out.print((char)img[j][i]);
                System.out.println();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
