# Deleting generated files
if [ -d target ] ; then
    rm -rf target/
fi

mkdir -p target

# Making jar archive with project in folder target/
javac -d target $(find src -name "*.java")

# Coping resources
cp -R src/resources target/

# Making jar archive with project in folder target/
jar -cfm ./target/images-to-chars-printer.jar ./src/manifest.txt -C target .

# Run built archive
java -jar ./target/images-to-chars-printer.jar . 0
