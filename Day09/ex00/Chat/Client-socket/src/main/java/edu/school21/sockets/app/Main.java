package edu.school21.sockets.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

@Parameters(separators = "=")
class Args{
    @Parameter(names="--server-port")
    public static int PORT = 1010;
}

public class Main {

    public static void main(String[] args) {
        JCommander.newBuilder().addObject(new Args()).build().parse(args);
        Scanner scanner = new Scanner(System.in);
        BufferedWriter writer ;
        try {
            Socket socket = new Socket(InetAddress.getLocalHost(), Args.PORT);
            System.out.println(socket.isConnected());
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String line;
                        while (true) {
                            if ((line = reader.readLine()) != null)
                                System.out.println(line);
                            if (line.equals("Successful!"))
                                System.exit(0);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();

            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String line;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                if (line.equals("42"))
                {
                    socket.close();
                    System.exit(0);
                }
                writer.append(line + "\n").flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
