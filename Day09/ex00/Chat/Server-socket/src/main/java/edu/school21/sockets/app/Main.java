package edu.school21.sockets.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import edu.school21.sockets.config.SocketsApplicationConfig;
import edu.school21.sockets.repositories.UsersRepository;
import edu.school21.sockets.server.Server;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;



public class Main {
    @Parameters(separators = "=")
    static public class Args{
        @Parameter(names="--port")
        public static int PORT = 1010;
    }
    public static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(SocketsApplicationConfig.class);

    public static void main(String[] args) {
        JCommander.newBuilder().addObject(new Args()).build().parse(args);
        DataSource dataSource = Main.CONTEXT.getBean(DataSource.class);
        JdbcTemplate template = new JdbcTemplate(dataSource);
        template.update("DROP TABLE IF EXISTS users CASCADE");
        template.update("CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, email varchar(100) NOT NUll UNIQUE, password varchar(100))");
        Server server = Server.getInstance(Args.PORT);
        server.start();
    }
}
