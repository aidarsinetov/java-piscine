package ex04;

import java.util.Scanner;

public class Program {
    private static final int MAX_CHAR_CODES = 65_535;
    private static final int NUMBER_OF_MAXS = 10;
    private static final int MAX_DIAGR_HEIGHT = 10;

    public static void main(String[] args) {
        int[] counters = new int[MAX_CHAR_CODES];
        char[] vocablurary = new char[MAX_CHAR_CODES];
        read(counters, vocablurary);

        int[] maxCounters = new int[NUMBER_OF_MAXS];
        char[] maxChars = new char[NUMBER_OF_MAXS];
        for (int j = 0; j < NUMBER_OF_MAXS && vocablurary[j] != 0; j++) {
            maxCounters[j] = 0;
            maxChars[j] = 0;
            for (int i = 0; vocablurary[i] != 0; i++) {
                if (counters[vocablurary[i]] > maxCounters[j]) {
                    if (j == 0 || maxCounters[j - 1] >= counters[vocablurary[i]] && findCharInArr(maxChars, vocablurary[i]) == -1) {
                        maxCounters[j] = counters[vocablurary[i]];
                        maxChars[j] = vocablurary[i];
                    }
                }
            }
        }
        sortSame(maxCounters, maxChars);
        printDiagram(maxCounters, maxChars);
    }

    private static void sortSame(int[] counters, char[] maxChars) {
        for (int k = 0; k < counters.length; k++) {
            for (int i = 1; i < counters.length - 1; i++) {
                if (counters[i] == counters[i + 1] && maxChars[i] > maxChars[i + 1]) {
                    char tmp = maxChars[i];
                    maxChars[i] = maxChars[i + 1];
                    maxChars[i + 1] = tmp;
                }
            }
        }
    }

    private static void read(int counters[], char vocablurary[]) {
        Scanner scanner = new Scanner(System.in);
        char charr[] = scanner.nextLine().toCharArray();
        int index = 0;
        for (int i = 0; i < charr.length; i++) {
            if (counters[charr[i]] == 0)
                vocablurary[index++] = charr[i];
            counters[charr[i]]++;
        }
    }

    private static int findCharInArr(char[] arr, char c) {
        for (int i = 0; i < arr.length; i++) {
            if (c == arr[i])
                return i;
        }
        return -1;
    }

    private static void printDiagram(int[] counters, char[] vocab) {
        int[] heights = new int[NUMBER_OF_MAXS];
        heights[0] = 10;
        for (int i = 1; i < NUMBER_OF_MAXS; i++)
            heights[i] = (counters[i] * MAX_DIAGR_HEIGHT) / counters[0];
        for (int i = MAX_DIAGR_HEIGHT; i >= 0; i--) {
            for (int j = 0; j < NUMBER_OF_MAXS && vocab[j] != 0; j++) {
                if (heights[j] == (i))
                    System.out.printf("%3d", counters[j]);
                if (i < heights[j])
                    System.out.printf("%3c", '#');
            }
            System.out.println();
        }
        for (int j = 0; j < vocab.length && vocab[j] != 0; j++)
            System.out.printf("%3c", vocab[j]);
        System.out.println();
    }
}